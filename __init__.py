from mycroft import MycroftSkill, intent_handler
from mycroft.skills.core import FallbackSkill
from . import openai_handler


class BhackGPT(FallbackSkill):
    def __init__(self):
        super(BhackGPT, self).__init__()
        self.open_ai_api_key = self.settings.get('open_ai_api_key', '')

    def initialize(self):
        self.register_fallback(self.handle_fallback, 10)
        self.settings_change_callback = self.on_settings_changed
        self.on_settings_changed()

    def on_settings_changed(self):
        self.open_ai_api_key = self.settings.get('open_ai_api_key', '')

    @intent_handler('bhackgpt.intent')
    def handle_bhackgpt(self, message):
        self.log.info(f"Sending this to OpenAI: {message.data['utterance']}")
        prompt, response = openai_handler.get_response(
            message.data['utterance'], self.settings.get('open_ai_api_key', ''))
        self.log.debug(f"The prompt was: {prompt}")
        self.log.debug(f"The response was: {response}")
        self.speak_dialog(response['choices'][0]['text'])
        return True

    def handle_fallback(self, message):
        return self.handle_bhackgpt(message)

    def shutdown(self):
        """
            Remove this skill from list of fallback skills.
        """
        self.remove_fallback(self.handle_fallback)
        super(BhackGPT, self).shutdown()


def create_skill():
    return BhackGPT()
