import os
import openai

initial_prompt = """
The following is a conversation with an AI assistant. 
The assistant is helpful, creative, clever, and very friendly.

Human: Hello, who are you?
AI: I am an AI created by OpenAI. How can I help you today?
Human: {specific_prompt}
AI:
"""


def get_response(specific_prompt: str, openai_api_key: str):
    # openai.api_key = os.getenv("OPENAI_API_KEY")
    openai.api_key = openai_api_key

    prompt = initial_prompt.format(specific_prompt=specific_prompt)
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=prompt,
        temperature=0.9,
        max_tokens=150,
        top_p=1,
        frequency_penalty=0.0,
        presence_penalty=0.6,
        stop=[" Human:", " AI:"]
    )

    return prompt, response
